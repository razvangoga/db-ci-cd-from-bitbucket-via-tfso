﻿select 'drop table dbo.'+name from sys.tables

select * from dbo.ApplicationVersions
select * from cicd.ExecutedUpdateScripts

drop table cicd.ExecutedUpdateScripts
drop table dbo.ApplicationAttachments
drop table dbo.Applications
drop table dbo.ApplicationVersions

delete from cicd.ExecutedUpdateScripts