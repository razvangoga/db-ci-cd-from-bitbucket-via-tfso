# PoC for DB CI/CD using VS DB Project, TFSO, AzureSQL

## Abstract

Implement a CI/CD pipeline for an Azure hosted SqlServer database starting from a Visual Studio Database Project and using Visual Studio Online Team Services to build and deploy the changes

Link to [PoC VSTS project](https://razvan-goga.visualstudio.com/ci-from-tfso)

## Requirements

1. ~~Start from a git hosted [Visual Studio Database Project](https://www.visualstudio.com/vs/features/ssdt/)~~
2. ~~Use [Visual Studio Team Services](https://www.visualstudio.com) for the build / deploy pipelines~~
3. ~~Allow for db schema changes~~
4. ~~Allow for db data changes~~
5. Allow rollbacks

## Implementation

### #1, #2, #3

A basic CI/CD for the schema can be implemented by folowing these 2 tutorials [1](https://www.youtube.com/watch?v=i3MmP4vekKY) or [2](https://www.youtube.com/watch?v=NFH_VWyfsKo).

The sourcecode can be on github, bitbuket or vsts.

In practice this will automatically do the same schema compare / schema update as it is manually possbile from inside Visual Studio

### #4

#### Option 1 - additional Azure SQL Database Deployment step

DMLs can be executed from a script via another **Azure SQL Database Deployment step** in the **Release** part of VSTS.

Steps:

* Create a **scripts** folder in the DBProject
* Add a SQL script file (the *not in build* type)
* Modify the build definition to also copy the scripts in the artifacts folder (clone the **Copy files** task that copies the dacpac)
* Modify the release definition to execute the script (clone the **dacpac** executing step and point it to an SQL script instead of the dacpac)

Pros:

* simple & fast
* uses only VSTS built in tasks

Cons:

* you have to somehow clear the script before the next release without deleting the script file because
* if the script file is not there (even if empty), the release **breaks**

#### Option 2 - Azure Powershell step

Execute a custom **Powershell script** in the **Release** part of VSTS that:

* [Adds](https://docs.microsoft.com/en-us/vsts/build-release/apps/cd/deploy-database-sqlscripts?view=vsts) the build machine IP to the Azure SQL Server Firewall
* Connects to the DB and executes the scripts
* Skips scripts that were run before (persists the run scripts somewhere)
* [Removes](https://docs.microsoft.com/en-us/vsts/build-release/apps/cd/deploy-database-sqlscripts?view=vsts) the build machine IP to the Azure SQL Server Firewall

In practice you will have to use the Azure-RM version of the cmdlets ([New-AzureRmSqlServerFirewallRule](https://docs.microsoft.com/en-us/powershell/module/azurerm.sql/new-azurermsqlserverfirewallrule) and [Remove-AzureRmSqlServerFirewallRule](https://docs.microsoft.com/en-us/powershell/module/azurerm.sql/remove-azurermsqlserverfirewallrule))
because the context in which VSTS runs your PowerShell sets up the Account / Subscription using the RM cmdlets [Add-AzureRMAccount](https://docs.microsoft.com/en-us/powershell/module/azurerm.profile/add-azurermaccount) and **Select-AzureRMSubscription** alias for [Set-AzureRmContext](https://docs.microsoft.com/en-us/powershell/module/azurerm.profile/set-azurermcontext). 

Pros:

* uses only VSTS built in tasks
* you can do (almost?) anything you can normally do in PowerShell
* by persisting the log of script files applied you do not need to do any after release manual actions (eg empty the update sql script or move it in another folder)

Cons:

* you have to write a PowerShell script - no GUI only config
* your db schema will be poluted by the log table or you will have to keep the log somewhere else in the cloud in a DB or some other webservice/webstorage (the release machine/agent is transient)