﻿CREATE TABLE [dbo].[Applications]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] nvarchar(100) not null,
	[ApplicationVersionId] int not null, 
    CONSTRAINT [FK_Applications_ApplicationVersions] FOREIGN KEY ([ApplicationVersionId]) REFERENCES [dbo].[ApplicationVersions]
)
