﻿CREATE TABLE [dbo].[ApplicationAttachments]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[ApplicationId] int not null,
	[Path] nvarchar(200) not null
	CONSTRAINT [FK_ApplicationAttachments_Applications] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[Applications]
)
