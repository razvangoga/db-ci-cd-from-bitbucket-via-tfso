﻿CREATE TABLE [cicd].[ExecutedUpdateScripts]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(200) NOT NULL, 
    [ExecutedAt] DATETIME NOT NULL DEFAULT getUTCDate() 
)
