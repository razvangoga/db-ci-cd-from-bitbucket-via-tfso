[CmdletBinding(DefaultParameterSetName = 'None')]
param
(
  [String] [Parameter(Mandatory = $true)] $ServerName,
  [String] [Parameter(Mandatory = $true)] $ResourceGroupName,
  [String] [Parameter(Mandatory = $true)] $ScriptsFolder,
  [String] [Parameter(Mandatory = $true)] $DBServerName,
  [String] [Parameter(Mandatory = $true)] $DBDatabaseName,
  [String] [Parameter(Mandatory = $true)] $DBUserName,
  [String] [Parameter(Mandatory = $true)] $DBPassword
)

function ReadFromDB([System.Data.SQLClient.SQLConnection] $connection, [String] $query) {
	[System.Collections.Generic.List[string]] $result = New-Object System.Collections.Generic.List[string]
	[System.Data.SQLClient.SQLCommand] $command = New-Object System.Data.SQLClient.SQLCommand
    $command.Connection = $connection
    $command.CommandText = $query
    $reader = $command.ExecuteReader()
    while ($reader.Read()) {
         $result.Add($reader.GetValue($1))
    }

	$reader.Close()
	return , $result
}

function WriteToDB([System.Data.SQLClient.SQLConnection] $connection, [String] $query) {
	[System.Data.SQLClient.SQLCommand] $command = New-Object System.Data.SQLClient.SQLCommand
    $command.Connection = $connection
    $command.CommandText = $query
    $result = $command.ExecuteNonQuery()
}

$azureFirewallName = "VSTORelease"

[string] $DBConnString = "Data Source=$DBServerName;Initial Catalog=$DBDatabaseName;Persist Security Info=True;User ID=$DBUserName;Password=$DBPassword;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True"

[string] $agentIP = (New-Object net.webclient).downloadstring("http://checkip.dyndns.com") -replace "[^\d\.]"
Write-Host "Executing from IP Address : $agentIP"

$rule = New-AzureRmSqlServerFirewallRule -StartIpAddress $agentIp -EndIpAddress $agentIp -FirewallRuleName $azureFirewallName -ServerName $ServerName -ResourceGroupName $ResourceGroupName
Write-Host "Firewall rule added"

[System.Data.SQLClient.SQLConnection] $connection = New-Object System.Data.SQLClient.SQLConnection
$connection.ConnectionString = $DBConnString
$connection.Open()

[System.Collections.Generic.List[string]] $alreadyExecutedScripts = ReadFromDB $connection "select Name from cicd.ExecutedUpdateScripts"

Get-ChildItem $ScriptsFolder -Filter *.sql | 
Foreach-Object {
	$scriptFileName = $_

	If ($alreadyExecutedScripts.Contains($scriptFileName)) {
		Write-Host "$scriptFileName - skipping (already executed)"
	}
	Else {
		Write-Host "$scriptFileName - running"
		$content = Get-Content "$ScriptsFolder\$scriptFileName"
		WriteToDB $connection $content
		WriteToDB $connection "insert into cicd.ExecutedUpdateScripts (Name) values ('$scriptFileName')"
		
		Write-Host "$scriptFileName - executed and marked as executed"
	}
}

$connection.Close()

$rule = Remove-AzureRmSqlServerFirewallRule -FirewallRuleName $AzureFirewallName -ServerName $ServerName -ResourceGroupName $ResourceGroupName
Write-Host "Firewall rule removed"

Write-Host "Done"